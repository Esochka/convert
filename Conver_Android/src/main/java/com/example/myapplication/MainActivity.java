package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v) {
        System.out.println("dsdsd");
        EditText text1 = findViewById(R.id.num1);
        EditText text2 = findViewById(R.id.num2);
        System.out.println(text1);
        int number1 = 0;
        int number2 = 0;
        EditText select11 = (EditText) findViewById(R.id.select1);
        EditText select22 = (EditText) findViewById(R.id.select2);
        String select1 = select11.getText().toString();
        String select2 = select22.getText().toString();


        boolean firstpol = false;
        boolean secondpol = false;

        if (!text1.getText().toString().equals("") && text2.getText().toString().equals("")) {
            secondpol = true;
            number1 = Integer.parseInt(text1.getText().toString());
        } else if (!text2.getText().toString().equals("") && text1.getText().toString().equals("")) {
            number2 = Integer.parseInt(text2.getText().toString());
            firstpol = true;
        }
        if (!text2.getText().toString().equals("") && !text1.getText().toString().equals("")) {
            text1.setText("");
            text2.setText("");

        } else {
            switch (select1) {
                case "m": {
                    switch (select2) {
                        case "km": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.001));
                            } else text1.setText(Double.toString(number2 / 0.001));
                            break;
                        }
                        case "mile": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.000621371));
                            } else text1.setText(Double.toString(number2 / 0.000621371));
                            break;
                        }
                        case "nautical mile": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.000539957));
                            } else text1.setText(Double.toString(number2 / 0.000539957));
                            break;
                        }
                        case "cable": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.0054));
                            } else text1.setText(Double.toString(number2 / 0.0054));
                            break;
                        }
                        case "league": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.000179986));
                            } else text1.setText(Double.toString(number2 / 0.000179986));
                            break;
                        }
                        case "foot": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 3.28084));
                            } else text1.setText(Double.toString(number2 / 3.28084));
                            break;
                        }
                        case "yard": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 1.09361));
                            } else text1.setText(Double.toString(number2 / 1.09361));
                            break;
                        }
                    }
                    break;
                }

                case "l": {
                    switch (select2) {
                        case "m^3": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.001));
                            } else text1.setText(Double.toString(number2 / 0.001));
                            break;
                        }
                        case "gallon": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.264172));
                            } else text1.setText(Double.toString(number2 / 0.264172));
                            break;
                        }
                        case "pint": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 2.11338));
                            } else text1.setText(Double.toString(number2 / 2.11338));
                            break;
                        }
                        case "quart": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 1.05669));
                            } else text1.setText(Double.toString(number2 / 1.05669));
                            break;
                        }
                        case "barrel": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.00852168));
                            } else text1.setText(Double.toString(number2 / 0.00852168));
                            break;
                        }
                        case "cubic foot": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 0.0353147));
                            } else text1.setText(Double.toString(number2 / 0.0353147));
                            break;
                        }
                        case "cubic inch": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 * 61.0237));
                            } else text1.setText(Double.toString(number2 / 61.0237));
                            break;
                        }
                    }

                    break;
                }

                case "sec": {
                    switch (select2) {
                        case "Min": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 60));
                            } else text1.setText(Double.toString(number2 * 60));
                            break;
                        }
                        case "Hour": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 3600));
                            } else text1.setText(Double.toString(number2 * 3600));
                            break;
                        }
                        case "Day": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 86400));
                            } else text1.setText(Double.toString(number2 * 86400));
                            break;
                        }
                        case "Week": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 604800));
                            } else text1.setText(Double.toString(number2 * 604800));
                            break;
                        }
                        case "Month": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 2628000));
                            } else text1.setText(Double.toString(number2 * 2628000));
                            break;
                        }
                        case "Astronomical Year": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 31540000));
                            } else text1.setText(Double.toString(number2 * 31540000));
                            break;
                        }
                        case "Third": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 / 315400000));
                            } else text1.setText(Double.toString(number2 * 315400000));
                            break;
                        }
                    }

                    break;
                }

                case "kg": {
                    switch (select2) {
                        case "g": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *1000));
                            } else text1.setText(Double.toString(number2 /1000));
                            break;
                        }
                        case "c": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *0.01));
                            } else text1.setText(Double.toString(number2 /0.01));
                            break;
                        }


                        case "carat": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *500));
                            } else text1.setText(Double.toString(number2 /500));
                            break;
                        }


                        case "eng pound": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *4.4));
                            } else text1.setText(Double.toString(number2 /4.4));
                            break;
                        }

                        case "pound": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *2.2));
                            } else text1.setText(Double.toString(number2 /2.2));
                            break;
                        }

                        case "stone": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *0.157473));
                            } else text1.setText(Double.toString(number2 /0.157473));
                            break;
                        }

                        case "rus pound": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *2.4));
                            } else text1.setText(Double.toString(number2 /2.4));
                            break;
                        }
                    }

                    break;
                }

                case "C": {
                    switch (select2) {
                        case "K": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 +273.15));
                            } else text1.setText(Double.toString(number2 -273.15));
                            break;
                        }
                        case "F": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *9/5+32));
                            } else text1.setText(Double.toString(number2 /9*5-32));
                            break;
                        }
                        case "Re": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *0.8));
                            } else text1.setText(Double.toString(number2 /0.8));
                            break;
                        }
                        case "Ro": {
                            if (secondpol) {
                                text2.setText(Double.toString((number1-7.5) *40/21));
                            } else text1.setText(Double.toString((number1+7.5) /40*21));
                            break;
                        }
                        case "Ra": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *1.8));
                            } else text1.setText(Double.toString(number1 /1.8));
                            break;
                        }
                        case "N": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *0.33));
                            } else text1.setText(Double.toString(number1 /0.33));
                            break;
                        }
                        case "D": {
                            if (secondpol) {
                                text2.setText(Double.toString(number1 *1.5+100));
                            } else text1.setText(Double.toString(number1 /1.5-100));
                            break;
                        }

                    }

                    break;
                }

            }
        }


    }
}
