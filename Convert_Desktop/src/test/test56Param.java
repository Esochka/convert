package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test56Param {
    private double valueA;
    private double expected;

    public test56Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1000000},
                {2000, 1000000 * 2}

        });
    }

    @Test
    public void lToMTest() {
        assertEquals(expected, new Exchenger().lToM(valueA), 0.1);
    }


}

