package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test42Param {
    private double valueA;
    private double expected;

    public test42Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 16.6},
                {2000, 33.3}

        });
    }

    @Test
    public void rusPoundToKsecToMinTestgTest() {
        assertEquals(expected,  new Exchenger().secToMin(valueA),0.1);
    }



}

