package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test9Param {
    private double valueA;
    private double expected;

    public test9Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1666666.67},
                {2000, 1666666.67*2}

        });
    }

    @Test
    public void mileToMTest() {
        assertEquals(expected,  new Exchenger().mileToM(valueA),0.1);
    }


}

