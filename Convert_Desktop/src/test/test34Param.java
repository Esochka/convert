package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test34Param {
    private double valueA;
    private double expected;

    public test34Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 2400},
                {2000, 2400*2}

        });
    }

    @Test
    public void kgToRusPoundTest() {
        assertEquals(expected,  new Exchenger().kgToRusPound(valueA),0.1);
    }



}

