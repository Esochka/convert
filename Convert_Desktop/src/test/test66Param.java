package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test66Param {
    private double valueA;
    private double expected;

    public test66Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1056},
                {2000, 1056 * 2}

        });
    }

    @Test
    public void quartToLTest() {
        assertEquals(expected, new Exchenger().quartToL(valueA), 0.1);
    }


}

