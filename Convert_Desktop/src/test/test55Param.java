package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test55Param {
    private double valueA;
    private double expected;

    public test55Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 3.154E11},
                {2000, 3.154E11 * 2}

        });
    }

    @Test
    public void thirdToSecTest() {
        assertEquals(expected, new Exchenger().thirdToSec(valueA), 0.1);
    }


}

