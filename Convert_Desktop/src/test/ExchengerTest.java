package test;

import org.junit.Test;
import sample.Exchenger;

import static org.junit.Assert.*;

public class ExchengerTest {

    @Test
    public void mToKmTest() {
        assertEquals(1, Exchenger.mToKm(1000),0.1);
    }
    @Test
    public void mToMileTest() {
        assertEquals(0.6,Exchenger.mToMile(1000),0.1);
    }
    @Test
    public void mToNauticalMileTest() {
        assertEquals(0.53,Exchenger.mToNauticalMile(1000),0.1);
    }
    @Test
    public void mToCableTest() {
        assertEquals(5.4,Exchenger.mToCable(1000),0.1);
    }
    @Test
    public void mToLeagueTest() {
        assertEquals(0.18,Exchenger.mToLeague(1000),0.1);
    }
    @Test
    public void mToFootTest() {
        assertEquals(3280,Exchenger.mToFoot(1000),0.1);
    }
    @Test
    public void mToYardTest() {
        assertEquals(1090,Exchenger.mToYard(1000),0.1);
    }
    @Test
    public void kmToMTest() {
        assertEquals(1000000,Exchenger.kmToM(1000),0.1);
    }
    @Test
    public void mileToMTest() {
        assertEquals(1666666.67,Exchenger.mileToM(1000),0.1);
    }
    @Test
    public void nauticalMileToMTest() {
        assertEquals(1851851.85,Exchenger.nauticalMileToM(1000),0.1);
    }
    @Test
    public void cableToMTest() {
        assertEquals(185185.185,Exchenger.cableToM(1000),0.1);
    }
    @Test
    public void footToMTest() {
        assertEquals(304.87,Exchenger.footToM(1000),0.1);
    }
    @Test
    public void yardToMTest() {
        assertEquals(917.43,Exchenger.yardToM(1000),0.1);
    }
    @Test
    public void cToKTest() {
        assertEquals(1273.15,Exchenger.cToK(1000),0.1);
    }
    @Test
    public void cToFTest() {
        assertEquals(1832,Exchenger.cToF(1000),0.1);
    }
    @Test
    public void cToReTest() {
        assertEquals(800,Exchenger.cToRe(1000),0.1);
    }
    @Test
    public void cToRoTest() {
        assertEquals(1890.47,Exchenger.cToRo(1000),0.1);
    }
    @Test
    public void cToRaTest() {
        assertEquals(1800,Exchenger.cToRa(1000),0.1);
    }
    @Test
    public void cToNTest() {
        assertEquals(330,Exchenger.cToN(1000),0.1);
    }
    @Test
    public void cToDTest() {
        assertEquals(1600,Exchenger.cToD(1000),0.1);
    }
    @Test
    public void kToCTest() {
        assertEquals(726.85,Exchenger.kToC(1000),0.1);
    }
    @Test
    public void fToCTest() {
        assertEquals(523.55,Exchenger.fToC(1000),0.1);
    }
    @Test
    public void reToCTest() {
        assertEquals(1250,Exchenger.reToC(1000),0.1);
    }
    @Test
    public void roToCTest() {
        assertEquals(528.93,Exchenger.roToC(1000),0.1);
    }
    @Test
    public void raToCTest() {
        assertEquals(555.555,Exchenger.raToC(1000),0.1);
    }
    @Test
    public void nToCTest() {
        assertEquals(3030.3,Exchenger.nToC(1000),0.1);
    }
    @Test
    public void dToCTest() {
        assertEquals(566.66,Exchenger.dToC(1000),0.1);
    }
    @Test
    public void kgToGTest() {
        assertEquals(1000000,Exchenger.kgToG(1000),0.1);
    }
    @Test
    public void kgToCTest() {
        assertEquals(10,Exchenger.kgToC(1000),0.1);
    }
    @Test
    public void kgToCaratTest() {
        assertEquals(500000,Exchenger.kgToCarat(1000),0.1);
    }
    @Test
    public void kgToEngPoundTest() {
        assertEquals(4400,Exchenger.kgToEngPound(1000),0.1);
    }
    @Test
    public void kgToPoundTest() {
        assertEquals(2200,Exchenger.kgToPound(1000),0.1);
    }
    @Test
    public void kgToStoneTest() {
        assertEquals(150,Exchenger.kgToStone(1000),0.1);
    }
    @Test
    public void kgToRusPoundTest() {
        assertEquals(2400,Exchenger.kgToRusPound(1000),0.1);
    }
    @Test
    public void gToKgTest() {
        assertEquals(1,Exchenger.gToKg(1000),0.1);
    }
    @Test
    public void cToKgTest() {
        assertEquals(100000,Exchenger.cToKg(1000),0.1);
    }
    @Test
    public void caratToKgTest() {
        assertEquals(2,Exchenger.caratToKg(1000),0.1);
    }
    @Test
    public void engPoundToKgTest() {
        assertEquals(227.27,Exchenger.engPoundToKg(1000),0.1);
    }
    @Test
    public void poundToKgTest() {
        assertEquals(454.454,Exchenger.poundToKg(1000),0.1);
    }
    @Test
    public void stoneToKgTest() {
        assertEquals(6666.66,Exchenger.stoneToKg(1000),0.1);
    }
    @Test
    public void rusPoundToKgTest() {
        assertEquals(416.66,Exchenger.rusPoundToKg(1000),0.1);
    }
    @Test
    public void secToMinTest() {
        assertEquals(16.6,Exchenger.secToMin(1000),0.1);
    }
    @Test
    public void secToHourTest() {
        assertEquals(0.27,Exchenger.secToHour(1000),0.1);
    }
    @Test
    public void secToDayTest() {
        assertEquals(0.011,Exchenger.secToDay(1000),0.1);
    }
    @Test
    public void secToWeekTest() {
        assertEquals(0.001,Exchenger.secToWeek(1000),0.1);
    }
    @Test
    public void secToMonthTest() {
        assertEquals(0.003,Exchenger.secToMonth(1000),0.1);
    }
    @Test
    public void secToAsronomicalYearTest() {
        assertEquals(0.00003,Exchenger.secToAsronomicalYear(1000),0.1);
    }
    @Test
    public void secToThirdTest() {
        assertEquals(0.00009,Exchenger.secToThird(1000),0.1);
    }
    @Test
    public void minToSecTest() {
        assertEquals(60000,Exchenger.minToSec(1000),0.1);
    }
    @Test
    public void hourToSecTest() {
        assertEquals(3600000,Exchenger.hourToSec(1000),0.1);
    }
    @Test
    public void dayToSecTest() {
        assertEquals(86400000,Exchenger.dayToSec(1000),0.1);
    }
    @Test
    public void weekToSecTest() {
        assertEquals(60480000,Exchenger.weekToSec(100),0.1);
    }
    @Test
    public void monthToSecTest() {
        assertEquals(262800000,Exchenger.monthToSec(100),0.1);
    }
    @Test
    public void asronomicalYearToSecTest() {
        assertEquals(315400000,Exchenger.asronomicalYearToSec(10),0.1);
    }
    @Test
    public void thirdToSecTest() {
        assertEquals(3.154E11,Exchenger.thirdToSec(1000),0.1);
    }
    @Test
    public void lToMTest() {
        assertEquals(1000000,Exchenger.lToM(1000),0.1);
    }
    @Test
    public void lToGallonTest() {
        assertEquals(3773.58,Exchenger.lToGallon(1000),0.1);
    }
    @Test
    public void lToPintTest() {
        assertEquals(473.93,Exchenger.lToPint(1000),0.1);
    }
    @Test
    public void lToQuartTest() {
        assertEquals(946.96,Exchenger.lToQuart(1000),0.1);
    }
    @Test
    public void lToBarrelTest() {
        assertEquals(117647,Exchenger.lToBarrel(1000),0.1);
    }
    @Test
    public void lToCubicFootTest() {
        assertEquals(28571.42,Exchenger.lToCubicFoot(1000),0.1);
    }
    @Test
    public void lToCubicInchTest() {
        assertEquals(16.38,Exchenger.lToCubicInch(1000),0.1);
    }
    @Test
    public void mToLTest() {
        assertEquals(1,Exchenger.mToL(1000),0.1);
    }
    @Test
    public void gallonToLTest() {
        assertEquals(265,Exchenger.gallonToL(1000),0.1);
    }
    @Test
    public void pintToLTest() {
        assertEquals(2110,Exchenger.pintToL(1000),0.1);
    }
    @Test
    public void quartToLTest() {
        assertEquals(1056,Exchenger.quartToL(1000),0.1);
    }
    @Test
    public void barrelToLTest() {
        assertEquals(8.5,Exchenger.barrelToL(1000),0.1);
    }
    @Test
    public void cubicFootToLTest() {
        assertEquals(35,Exchenger.cubicFootToL(1000),0.1);
    }
    @Test
    public void cubicInchToLTest() {
        assertEquals(61020,Exchenger.cubicInchToL(1000),0.1);
    }



}