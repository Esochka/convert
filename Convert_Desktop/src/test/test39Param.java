package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test39Param {
    private double valueA;
    private double expected;

    public test39Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 454.454},
                {2000, 909.09}

        });
    }

    @Test
    public void poundToKgTest() {
        assertEquals(expected,  new Exchenger().poundToKg(valueA),0.1);
    }



}

