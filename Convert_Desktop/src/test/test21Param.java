package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test21Param {
    private double valueA;
    private double expected;

    public test21Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 726.85},
                {2000, 1726.85}

        });
    }

    @Test
    public void kToCTest() {
        assertEquals(expected,  new Exchenger().kToC(valueA),0.1);
    }


}

