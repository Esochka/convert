package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test8Param {
    private double valueA;
    private double expected;

    public test8Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1000000},
                {2000, 2000000}

        });
    }

    @Test
    public void kmToMTest() {
        assertEquals(expected,  new Exchenger().kmToM(valueA),0.1);
    }


}

