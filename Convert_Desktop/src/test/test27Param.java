package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test27Param {
    private double valueA;
    private double expected;

    public test27Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 566.66},
                {2000, 1233.3333333333333}

        });
    }

    @Test
    public void dToCTest() {
        assertEquals(expected,  new Exchenger().dToC(valueA),0.1);
    }


}

