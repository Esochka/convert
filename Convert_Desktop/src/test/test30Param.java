package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test30Param {
    private double valueA;
    private double expected;

    public test30Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 500000},
                {2000, 500000*2}

        });
    }

    @Test
    public void kgToCaratTest() {
        assertEquals(expected,  new Exchenger().kgToCarat(valueA),0.1);
    }



}

