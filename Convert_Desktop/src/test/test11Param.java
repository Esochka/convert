package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test11Param {
    private double valueA;
    private double expected;

    public test11Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 185185.185},
                {2000, 185185.185*2}

        });
    }

    @Test
    public void cableToMTest() {
        assertEquals(expected,  new Exchenger().cableToM(valueA),0.1);
    }


}

