package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test14Param {
    private double valueA;
    private double expected;

    public test14Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1273.15},
                {2000, 2273.15}

        });
    }

    @Test
    public void cToKTest() {
        assertEquals(expected,  new Exchenger().cToK(valueA),0.1);
    }


}

