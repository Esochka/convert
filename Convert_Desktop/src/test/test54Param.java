package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test54Param {
    private double valueA;
    private double expected;

    public test54Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {10, 315400000},
                {20, 315400000 * 2}

        });
    }

    @Test
    public void asronomicalYearToSecTest() {
        assertEquals(expected, new Exchenger().asronomicalYearToSec(valueA), 0.1);
    }


}

