package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test52Param {
    private double valueA;
    private double expected;

    public test52Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {100, 60480000},
                {200, 60480000 * 2}

        });
    }

    @Test
    public void weekToSecTest() {
        assertEquals(expected, new Exchenger().weekToSec(valueA), 0.1);
    }


}

