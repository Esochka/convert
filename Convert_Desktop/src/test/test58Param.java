package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test58Param {
    private double valueA;
    private double expected;

    public test58Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 473.93},
                {2000, 473.93 * 2}

        });
    }

    @Test
    public void lToPintTest() {
        assertEquals(expected, new Exchenger().lToPint(valueA), 0.1);
    }


}

