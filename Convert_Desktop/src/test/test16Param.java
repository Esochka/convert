package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test16Param {
    private double valueA;
    private double expected;

    public test16Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 800},
                {2000, 800*2}

        });
    }

    @Test
    public void cToReTest() {
        assertEquals(expected,  new Exchenger().cToRe(valueA),0.1);
    }


}

