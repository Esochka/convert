package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test25Param {
    private double valueA;
    private double expected;

    public test25Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 555.555},
                {2000, 555.555*2}

        });
    }

    @Test
    public void raToCTest() {
        assertEquals(expected,  new Exchenger().raToC(valueA),0.1);
    }


}

