package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExchengerTest {

    @Test
    public void mToKmTest() {
        assertEquals(1,Exchenger.mToKm(1000),0.1);
    }
    @Test
    public void mToMileTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToMile(num);

        //then
        assertEquals(0.6,res,0.1);
    }
    @Test
    public void mToNauticalMileTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToNauticalMile(num);

        //then
        assertEquals(0.53,res,0.1);
    }
    @Test
    public void mToCableTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToCable(num);

        //then
        assertEquals(5.4,res,0.1);
    }
    @Test
    public void mToLeagueTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToLeague(num);

        //then
        assertEquals(0.18,res,0.1);
    }
    @Test
    public void mToFootTest() {

        //given
        double num = 1000;

        //when
        double res = Exchenger.mToFoot(num);

        //then
        assertEquals(3280,res,0.1);
    }
    @Test
    public void mToYardTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToYard(num);

        //then
        assertEquals(1090,res,0.1);
    }
    @Test
    public void kmToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kmToM(num);

        //then
        assertEquals(1000000,res,0.1);
    }
    @Test
    public void mileToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mileToM(num);

        //then
        assertEquals(1666666.67,res,0.1);
    }
    @Test
    public void nauticalMileToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.nauticalMileToM(num);

        //then
        assertEquals(1851851.85,res,0.1);
    }
    @Test
    public void cableToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cableToM(num);

        //then
        assertEquals(185185.185,res,0.1);
    }
    @Test
    public void footToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.footToM(num);

        //then
        assertEquals(304.87,res,0.1);
    }
    @Test
    public void yardToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.yardToM(num);

        //then
        assertEquals(917.43,res,0.1);
    }
    @Test
    public void cToKTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToK(num);

        //then
        assertEquals(1273.15,res,0.1);
    }
    @Test
    public void cToFTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToF(num);

        //then
        assertEquals(1832,res,0.1);
    }
    @Test
    public void cToReTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToRe(num);

        //then
        assertEquals(800,res,0.1);
    }
    @Test
    public void cToRoTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToRo(num);

        //then
        assertEquals(1890.47,res,0.1);
    }
    @Test
    public void cToRaTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToRa(num);

        //then
        assertEquals(1800,res,0.1);
    }
    @Test
    public void cToNTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToN(num);

        //then
        assertEquals(330,res,0.1);
    }
    @Test
    public void cToDTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToD(num);

        //then
        assertEquals(1600,res,0.1);
    }
    @Test
    public void kToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kToC(num);

        //then
        assertEquals(726.85,res,0.1);
    }
    @Test
    public void fToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.fToC(num);

        //then
        assertEquals(523.55,res,0.1);
    }
    @Test
    public void reToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.reToC(num);

        //then
        assertEquals(1250,res,0.1);
    }
    @Test
    public void roToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.roToC(num);

        //then
        assertEquals(528.93,res,0.1);
    }
    @Test
    public void raToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.raToC(num);

        //then
        assertEquals(555.555,res,0.1);
    }
    @Test
    public void nToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.nToC(num);

        //then
        assertEquals(3030.3,res,0.1);
    }
    @Test
    public void dToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.dToC(num);

        //then
        assertEquals(566.66,res,0.1);
    }
    @Test
    public void kgToGTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToG(num);

        //then
        assertEquals(1000000,res,0.1);
    }
    @Test
    public void kgToCTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToC(num);

        //then
        assertEquals(10,res,0.1);
    }
    @Test
    public void kgToCaratTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToCarat(num);

        //then
        assertEquals(500000,res,0.1);
    }
    @Test
    public void kgToEngPoundTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToEngPound(num);

        //then
        assertEquals(4400,res,0.1);
    }
    @Test
    public void kgToPoundTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToPound(num);

        //then
        assertEquals(2200,res,0.1);
    }
    @Test
    public void kgToStoneTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToStone(num);

        //then
        assertEquals(150,res,0.1);
    }
    @Test
    public void kgToRusPoundTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.kgToRusPound(num);

        //then
        assertEquals(2400,res,0.1);
    }
    @Test
    public void gToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.gToKg(num);

        //then
        assertEquals(1,res,0.1);
    }
    @Test
    public void cToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cToKg(num);

        //then
        assertEquals(100000,res,0.1);
    }
    @Test
    public void caratToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.caratToKg(num);

        //then
        assertEquals(2,res,0.1);
    }
    @Test
    public void engPoundToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.engPoundToKg(num);

        //then
        assertEquals(227.27,res,0.1);
    }
    @Test
    public void poundToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.poundToKg(num);

        //then
        assertEquals(454.454,res,0.1);
    }
    @Test
    public void stoneToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.stoneToKg(num);

        //then
        assertEquals(6666.66,res,0.1);
    }
    @Test
    public void rusPoundToKgTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.rusPoundToKg(num);

        //then
        assertEquals(416.66,res,0.1);
    }
    @Test
    public void secToMinTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToMin(num);

        //then
        assertEquals(16.6,res,0.1);
    }
    @Test
    public void secToHourTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToHour(num);

        //then
        assertEquals(0.27,res,0.1);
    }
    @Test
    public void secToDayTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToDay(num);

        //then
        assertEquals(0.011,res,0.1);
    }
    @Test
    public void secToWeekTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToWeek(num);

        //then
        assertEquals(0.001,res,0.1);
    }
    @Test
    public void secToMonthTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToMonth(num);

        //then
        assertEquals(0.003,res,0.1);
    }
    @Test
    public void secToAsronomicalYearTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToAsronomicalYear(num);

        //then
        assertEquals(0.00003,res,0.1);
    }
    @Test
    public void secToThirdTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.secToThird(num);

        //then
        assertEquals(0.00009,res,0.1);
    }
    @Test
    public void minToSecTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.minToSec(num);

        //then
        assertEquals(60000,res,0.1);
    }
    @Test
    public void hourToSecTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.hourToSec(num);

        //then
        assertEquals(3600000,res,0.1);
    }
    @Test
    public void dayToSecTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.dayToSec(num);

        //then
        assertEquals(86400000,res,0.1);
    }
    @Test
    public void weekToSecTest() {
        //given
        double num = 100;

        //when
        double res = Exchenger.weekToSec(num);

        //then
        assertEquals(60480000,res,0.1);
    }
    @Test
    public void monthToSecTest() {
        //given
        double num = 100;

        //when
        double res = Exchenger.monthToSec(num);

        //then
        assertEquals(262800000,res,0.1);
    }
    @Test
    public void asronomicalYearToSecTest() {
        //given
        double num = 10;

        //when
        double res = Exchenger.asronomicalYearToSec(num);

        //then
        assertEquals(315400000,res,0.1);
    }
    @Test
    public void thirdToSecTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.thirdToSec(num);

        //then
        assertEquals(3.154E11,res,0.1);
    }
    @Test
    public void lToMTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToM(num);

        //then
        assertEquals(1000000,res,0.1);
    }
    @Test
    public void lToGallonTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToGallon(num);

        //then
        assertEquals(3773.58,res,0.1);
    }
    @Test
    public void lToPintTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToPint(num);

        //then
        assertEquals(473.93,res,0.1);
    }
    @Test
    public void lToQuartTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToQuart(num);

        //then
        assertEquals(946.96,res,0.1);
    }
    @Test
    public void lToBarrelTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToBarrel(num);

        //then
        assertEquals(117647,res,0.1);
    }
    @Test
    public void lToCubicFootTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToCubicFoot(num);

        //then
        assertEquals(28571.42,res,0.1);
    }
    @Test
    public void lToCubicInchTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.lToCubicInch(num);

        //then
        assertEquals(16.38,res,0.1);
    }
    @Test
    public void mToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.mToL(num);

        //then
        assertEquals(1,res,0.1);
    }
    @Test
    public void gallonToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.gallonToL(num);

        //then
        assertEquals(265,res,0.1);
    }
    @Test
    public void pintToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.pintToL(num);

        //then
        assertEquals(2110,res,0.1);
    }
    @Test
    public void quartToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.quartToL(num);

        //then
        assertEquals(1056,res,0.1);
    }
    @Test
    public void barrelToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.barrelToL(num);

        //then
        assertEquals(8.5,res,0.1);
    }
    @Test
    public void cubicFootToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cubicFootToL(num);

        //then
        assertEquals(35,res,0.1);
    }
    @Test
    public void cubicInchToLTest() {
        //given
        double num = 1000;

        //when
        double res = Exchenger.cubicInchToL(num);

        //then
        assertEquals(61020,res,0.1);
    }



}