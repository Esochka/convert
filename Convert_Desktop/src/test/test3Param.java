package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test3Param {
    private double valueA;
    private double expected;

    public test3Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 0.53},
                {2000, 1.06}

        });
    }

    @Test
    public void mToNauticalMileTest() {
        assertEquals(expected,  new Exchenger().mToNauticalMile(valueA),0.1);
    }


}

