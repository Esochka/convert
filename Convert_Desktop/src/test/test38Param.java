package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test38Param {
    private double valueA;
    private double expected;

    public test38Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 227.27},
                {2000, 227.27*2}

        });
    }

    @Test
    public void engPoundToKgTest() {
        assertEquals(expected,  new Exchenger().engPoundToKg(valueA),0.1);
    }



}

