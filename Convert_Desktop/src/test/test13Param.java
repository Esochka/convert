package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test13Param {
    private double valueA;
    private double expected;

    public test13Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 917.43},
                {2000, 917.43*2}

        });
    }

    @Test
    public void yardToMTest() {
        assertEquals(expected,  new Exchenger().yardToM(valueA),0.1);
    }


}
