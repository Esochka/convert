package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test17Param {
    private double valueA;
    private double expected;

    public test17Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 1890.47},
                {2000, 3795.23}

        });
    }

    @Test
    public void cToRoTest() {
        assertEquals(expected,  new Exchenger().cToRo(valueA),0.1);
    }


}
