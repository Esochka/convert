package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test67Param {
    private double valueA;
    private double expected;

    public test67Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 8.5},
                {2000, 8.5 * 2}

        });
    }

    @Test
    public void barrelToLTest() {
        assertEquals(expected, new Exchenger().barrelToL(valueA), 0.1);
    }


}

