package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test4Param {
    private double valueA;
    private double expected;

    public test4Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 5.4},
                {2000, 10.8}

        });
    }

    @Test
    public void mToCableTest() {
        assertEquals(expected,  new Exchenger().mToCable(valueA),0.1);
    }


}


