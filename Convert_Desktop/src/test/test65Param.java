package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test65Param {
    private double valueA;
    private double expected;

    public test65Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 2110},
                {2000, 2110 * 2}

        });
    }

    @Test
    public void pintToLTest() {
        assertEquals(expected, new Exchenger().pintToL(valueA), 0.1);
    }


}

