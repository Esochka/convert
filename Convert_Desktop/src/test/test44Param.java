package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test44Param {
    private double valueA;
    private double expected;

    public test44Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 0.011},
                {2000, 0.011 * 2}

        });
    }

    @Test
    public void secToDayTest() {
        assertEquals(expected, new Exchenger().secToDay(valueA), 0.1);
    }


}


