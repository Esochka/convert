package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test60Param {
    private double valueA;
    private double expected;

    public test60Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 117647},
                {2000, 235294.1176470588}

        });
    }

    @Test
    public void lToBarrelTest() {
        assertEquals(expected, new Exchenger().lToBarrel(valueA), 0.1);
    }


}

