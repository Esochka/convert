package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sample.Exchenger;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class test51Param {
    private double valueA;
    private double expected;

    public test51Param(double valueA, double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {1000, 86400000},
                {2000, 86400000 * 2}

        });
    }

    @Test
    public void dayToSecTest() {
        assertEquals(expected, new Exchenger().dayToSec(valueA), 0.1);
    }


}

