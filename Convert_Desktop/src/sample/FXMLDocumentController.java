package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class FXMLDocumentController {


    @FXML
    private Button button1;

    @FXML
    private Button button2;

    @FXML
    private Button button3;

    @FXML
    private Button button4;

    @FXML
    private Button button5;

    @FXML
    private Button button6;

    @FXML
    private TextField text1;

    @FXML
    private TextField text2;

    @FXML
    private ComboBox<String> select1;

    @FXML
    private ComboBox<String> select2;

    @FXML
    void handleButtonAction(ActionEvent event) {

        select1.getItems().removeAll("m", "C", "l", "sec", "kg");
        select2.getItems().removeAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra", "N", "D", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound");
        select1.getItems().addAll("m");
        select2.getItems().addAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard");


    }

    @FXML
    void handleButtonAction2(ActionEvent event) {
        select1.getItems().removeAll("m", "C", "l", "sec", "kg");
        select2.getItems().removeAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra", "N", "D", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week", "Month", "Astronomical Year", "Third", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound");
        select1.getItems().addAll("C");
        select2.getItems().addAll("K", "F", "Re", "Ro", "Ra", "N", "D");

    }

    @FXML
    void handleButtonAction3(ActionEvent event) {
        select1.getItems().removeAll("m", "C", "l", "sec", "kg");
        select2.getItems().removeAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra", "N", "D", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week", "Month", "Astronomical Year", "Third", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound");
        select1.getItems().addAll("l");
        select2.getItems().addAll("m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch");

    }

    @FXML
    void handleButtonAction4(ActionEvent event) {
        select1.getItems().removeAll("m", "C", "l", "sec", "kg");
        select2.getItems().removeAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra", "N", "D", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week", "Month", "Astronomical Year", "Third", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound");
        select1.getItems().addAll("sec");
        select2.getItems().addAll("Min", "Hour", "Day", "Week", "Month", "Astronomical Year", "Third");

    }

    @FXML
    void handleButtonAction5(ActionEvent event) {
        select1.getItems().removeAll("m", "C", "l", "sec", "kg");
        select2.getItems().removeAll("km", "mile", "nautical mile", "cable", "league", "foot", "yard", "K", "F", "Re", "Ro", "Ra", "N", "D", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch", "Min", "Hour", "Day", "Week", "Month", "Astronomical Year", "Third", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound");
        select1.getItems().addAll("kg");
        select2.getItems().addAll("g", "c", "carat", "eng pound", "pound", "stone", "rus pound");

    }

    @FXML
    void handleButtonAction6(ActionEvent event) {
        try {
            boolean firstpol = false;
            boolean secondpol = false;

            if (!text1.getText().equals("") && text2.getText().equals("")) {
                secondpol = true;
            } else if (!text2.getText().equals("") && text1.getText().equals("")) {

                firstpol = true;
            } else {
                text1.setText("");
                text2.setText("");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Eror");
                alert.setContentText("Заповніть одне поле");
                alert.showAndWait();
            }
            switch (select1.getValue()) {
                case "m": {
                    switch (select2.getValue()) {
                        case "km": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToKm(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.kmToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "mile": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToMile(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.mileToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "nautical mile": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToNauticalMile(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.nauticalMileToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "cable": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToCable(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.cableToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "league": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToLeague(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.leagueToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "foot": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToFoot(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.footToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "yard": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.mToYard(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.yardToM(Double.parseDouble(text2.getText()))));
                            break;
                        }
                    }
                    break;
                }

                case "l": {
                    switch (select2.getValue()) {
                        case "m^3": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToM(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.mToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "gallon": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToGallon(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.gallonToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "pint": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToPint(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.pintToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "quart": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToQuart(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.quartToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "barrel": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToBarrel(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.barrelToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "cubic foot": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToCubicFoot(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.cubicFootToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "cubic inch": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.lToCubicInch(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.cubicInchToL(Double.parseDouble(text2.getText()))));
                            break;
                        }
                    }

                    break;
                }

                case "sec": {
                    switch (select2.getValue()) {
                        case "Min": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToMin(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.minToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Hour": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToHour(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.hourToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Day": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToDay(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.dayToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Week": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToWeek(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.weekToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Month": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToMonth(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.monthToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Astronomical Year": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToAsronomicalYear(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.asronomicalYearToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Third": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.secToThird(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.thirdToSec(Double.parseDouble(text2.getText()))));
                            break;
                        }
                    }

                    break;
                }

                case "kg": {
                    switch (select2.getValue()) {
                        case "g": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToG(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.gToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }


                        case "c": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToC(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.cToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }


                        case "carat": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToCarat(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.caratToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }


                        case "eng pound": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToEngPound(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.engPoundToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }

                        case "pound": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToPound(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.poundToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }

                        case "stone": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToStone(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.stoneToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }

                        case "rus pound": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.kgToRusPound(Double.parseDouble(text1.getText()))));
                            } else
                                text1.setText(String.valueOf(Exchenger.rusPoundToKg(Double.parseDouble(text2.getText()))));
                            break;
                        }
                    }

                    break;
                }

                case "C": {
                    switch (select2.getValue()) {
                        case "K": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToK(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.kToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "F": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToF(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.fToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Re": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToRe(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.reToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Ro": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToRo(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.roToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "Ra": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToRa(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.raToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "N": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToN(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.nToC(Double.parseDouble(text2.getText()))));
                            break;
                        }
                        case "D": {
                            if (secondpol) {
                                text2.setText(String.valueOf(Exchenger.cToD(Double.parseDouble(text1.getText()))));
                            } else text1.setText(String.valueOf(Exchenger.dToC(Double.parseDouble(text2.getText()))));
                            break;
                        }

                    }

                    break;
                }

            }

        } catch (Exception e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Eror");
            alert.setContentText("Виникла помилка) спробуйте ще раз");
            alert.showAndWait();
        }


    }


    @FXML
    void initialize() {

    }
}

